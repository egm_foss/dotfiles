#!/usr/bin/env bash
BASEDIR=$(dirname $0)
cd $BASEDIR

ln -s ${PWD}/bash/bashrc ~/.bashrc
ln -s ${PWD}/alias/aliases ~/.aliases
ln -s ${PWD}/alacritty/alacritty.yml ~/.alacritty.yml
ln -s ${PWD}/git/gitconfig ~/.gitconfig

